const axios = require("axios");

module.exports = class GameTopChartsService {
  constructor(options) {
    this.providerAccessToken = options.providerAccessToken;
  }

  async getTopGamesByPlatform(options, acc = 0) {
    let platformFetcher;

    if (options.platform === "ios") {
      platformFetcher = this._getTopAppStoreGameCharts.bind(this);
    } else if (options.platform === "android") {
      platformFetcher = this._getTopPlayStoreGameCharts.bind(this);
    } else {
      return [];
    }

    const { games, hasNext } = await platformFetcher(options);

    acc += games.length;

    if (acc < options.limit && hasNext) {
      return games.concat(
        await this.getTopGamesByPlatform(
          { ...options, page: options.page ? options.page + 1 : 1 },
          acc
        )
      );
    }

    return games;
  }

  async _getTopAppStoreGameCharts(options) {
    const GAMES_CATEGORY_KEY = 6014;

    const { data: charts } = await axios.get(
      "https://data.42matters.com/api/v2.0/ios/apps/top_appstore_charts.json",
      {
        params: {
          access_token: this.providerAccessToken,
          primaryGenreId: GAMES_CATEGORY_KEY,
          limit: options.limit,
          page: options.page,
        },
      }
    );

    return {
      games: charts.app_list.map(this._format42MatterIOSAppObject),
      hasNext: charts.has_next,
    };
  }

  async _getTopPlayStoreGameCharts(options) {
    const GAMES_CATEGORY_KEY = "GAME";

    const { data: charts } = await axios.get(
      "https://data.42matters.com/api/v2.0/android/apps/top_google_charts.json",
      {
        params: {
          access_token: this.providerAccessToken,
          cat_key: GAMES_CATEGORY_KEY,
          limit: options.limit,
          page: options.page,
        },
      }
    );

    return {
      games: charts.app_list.map(this._format42MatterAndroidAppObject),
      hasNext: charts.has_next,
    };
  }

  _format42MatterAndroidAppObject(androidAppObject) {
    return {
      publisherId: androidAppObject.developer,
      name: androidAppObject.title,
      platform: "android",
      storeId: androidAppObject.package_name,
      bundleId: androidAppObject.package_name,
      appVersion: androidAppObject.version,
      isPublished: androidAppObject.market_status === "PUBLISHED",
    };
  }

  _format42MatterIOSAppObject(iOSAppObject) {
    return {
      publisherId: iOSAppObject.sellerName,
      name: iOSAppObject.trackCensoredName,
      platform: "ios",
      storeId: iOSAppObject.trackId,
      bundleId: iOSAppObject.bundleId,
      appVersion: iOSAppObject.version,
      isPublished: iOSAppObject.market_status === "PUBLISHED",
    };
  }
};
