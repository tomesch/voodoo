const request = require('supertest');
var assert = require('assert');
const app = require('../index');

/**
 * Testing create game endpoint
 */
describe('POST /api/games', function () {
    let data = {
        publisherId: "1234567890",
        name: "Test App",
        platform: "ios",
        storeId: "1234",
        bundleId: "test.bundle.id",
        appVersion: "1.0.0",
        isPublished: true
    }
    it('respond with 200 and an object that matches what we created', function (done) {
        request(app)
            .post('/api/games')
            .send(data)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, result) => {
                if (err) return done(err);
                assert.strictEqual(result.body.publisherId, '1234567890');
                assert.strictEqual(result.body.name, 'Test App');
                assert.strictEqual(result.body.platform, 'ios');
                assert.strictEqual(result.body.storeId, '1234');
                assert.strictEqual(result.body.bundleId, 'test.bundle.id');
                assert.strictEqual(result.body.appVersion, '1.0.0');
                assert.strictEqual(result.body.isPublished, true);
                done();
            });
    });
});

/**
 * Testing get all games endpoint
 */
describe('GET /api/games', function () {
    it('respond with json containing a list that includes the game we just created', function (done) {
        request(app)
            .get('/api/games')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, result) => {
                if (err) return done(err);
                assert.strictEqual(result.body[0].publisherId, '1234567890');
                assert.strictEqual(result.body[0].name, 'Test App');
                assert.strictEqual(result.body[0].platform, 'ios');
                assert.strictEqual(result.body[0].storeId, '1234');
                assert.strictEqual(result.body[0].bundleId, 'test.bundle.id');
                assert.strictEqual(result.body[0].appVersion, '1.0.0');
                assert.strictEqual(result.body[0].isPublished, true);
                done();
            });
    });
});


/**
 * Testing update game endpoint
 */
describe('PUT /api/games/1', function () {
    let data = {
        id : 1,
        publisherId: "999000999",
        name: "Test App Updated",
        platform: "android",
        storeId: "5678",
        bundleId: "test.newBundle.id",
        appVersion: "1.0.1",
        isPublished: false
    }
    it('respond with 200 and an updated object', function (done) {
        request(app)
            .put('/api/games/1')
            .send(data)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, result) => {
                if (err) return done(err);
                assert.strictEqual(result.body.publisherId, '999000999');
                assert.strictEqual(result.body.name, 'Test App Updated');
                assert.strictEqual(result.body.platform, 'android');
                assert.strictEqual(result.body.storeId, '5678');
                assert.strictEqual(result.body.bundleId, 'test.newBundle.id');
                assert.strictEqual(result.body.appVersion, '1.0.1');
                assert.strictEqual(result.body.isPublished, false);
                done();
            });
    });
});

/**
 * Testing update game endpoint
 */
describe('DELETE /api/games/1', function () {
    it('respond with 200', function (done) {
        request(app)
            .delete('/api/games/1')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err) => {
                if (err) return done(err);
                done();
            });
    });
});

/**
 * Testing get all games endpoint
 */
describe('GET /api/games', function () {
    it('respond with json containing no games', function (done) {
        request(app)
            .get('/api/games')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, result) => {
                if (err) return done(err);
                assert.strictEqual(result.body.length, 0);
                done();
            });
    });
});

/**
 * Testing games search endpoint
 */
describe("GET /api/games/search", function () {
  const testGames = [
    {
      publisherId: "1234567890",
      name: "Foo Bar",
      platform: "ios",
      storeId: "1234",
      bundleId: "test.bundle.id",
      appVersion: "1.0.0",
      isPublished: true,
    },
    {
      publisherId: "1234567890",
      name: "Bar Foo",
      platform: "ios",
      storeId: "1234",
      bundleId: "test.bundle.id",
      appVersion: "1.0.0",
      isPublished: true,
    },
    {
      publisherId: "1234567890",
      name: "Buzz Zar",
      platform: "android",
      storeId: "1234",
      bundleId: "test.bundle.id",
      appVersion: "1.0.0",
      isPublished: true,
    },
  ];

  beforeEach(() =>
    Promise.all(
      testGames.map((game) =>
        request(app).post("/api/games").send(game).expect(200)
      )
    )
  );

  afterEach(async () => {
    const { body: existingGames } = await request(app)
      .get("/api/games")
      .expect(200);

    return Promise.all(
      existingGames.map(({ id }) =>
        request(app).delete(`/api/games/${id}`).expect(200)
      )
    );
  });

  it("responds with all games for ios", function (done) {
    const platform = "ios";

    request(app)
      .post("/api/games/search")
      .send({
        name: "",
        platform,
      })
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .end((err, result) => {
        console.log(result.body);
        if (err) return done(err);
        assert.equal(
          result.body.length,
          testGames.filter((game) => game.platform === platform).length
        );
        assert.ok(result.body.every((game) => game.platform === platform));

        done();
      });
  });

  it("responds with all games when no platform and no search pattern is defined", function (done) {
    request(app)
      .post("/api/games/search")
      .send({
        name: "",
        platform: "",
      })
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .end((err, result) => {
        console.log(result.body);
        if (err) return done(err);
        assert.equal(result.body.length, testGames.length);

        done();
      });
  });

  it("responds with all games matching the search pattern", function (done) {
    const searchPattern = "foo";

    request(app)
      .post("/api/games/search")
      .send({
        name: searchPattern,
        platform: "",
      })
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .end((err, result) => {
        console.log(result.body);
        if (err) return done(err);
        assert.equal(
          result.body.length,
          testGames.filter((game) =>
            game.name.toLocaleLowerCase().includes(searchPattern)
          ).length
        );

        done();
      });
  });
});