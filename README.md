# Candidate Takehome Exercise
This is a simple backend engineer take-home test to help assess candidate skills and practices.  We appreciate your interest in Voodoo and have created this exercise as a tool to learn more about how you practice your craft in a realistic environment.  This is a test of your coding ability, but more importantly it is also a test of your overall practices.

If you are a seasoned Node.js developer, the coding portion of this exercise should take no more than 1-2 hours to complete.  Depending on your level of familiarity with Node.js, Express, and Sequelize, it may not be possible to finish in 2 hours, but you should not spend more than 2 hours.  

We value your time, and you should too.  If you reach the 2 hour mark, save your progress and we can discuss what you were able to accomplish.  We do not expect you to "go the extra mile" and spend more than 2 hours on this test.  You will not get extra credit if you spend more than 2 hours.

The theory portions of this test are more open-ended.  It is up to you how much time you spend addressing these questions.  We recommend spending less than 1 hour.  For the record, we are not testing to see how much free time you have, so there will be no extra credit for monumental time investments.  We are looking for concise, clear answers that demonstrate domain expertise.

# Project Overview
This project is a simple game database and consists of 2 components.  

The first component is a Vue.js UI that communicates with an API and renders data in a simple browser-based UI.

The second component is an Express-based API server that queries and delivers data from a SQL-lite data source, using the Sequelize ORM.

This code is not necessarily representative of what you would find in a Voodoo production-ready codebase.  However, this type of stack is in regular use at Voodoo.

# Project Setup
You will need to have Node.js, NPM, and git installed locally.  You should not need anything else.

To get started, initialize a local git repo by going into the root of this project and running `git init`.  Then run `git add .` to add all of the relevant files.  Then `git commit` to complete the repo setup.  You will send us this repo as your final product.
  
Next, in a terminal, run `npm install` from the project root to initialize your dependencies.

Finally, to start the application, navigate to the project root in a terminal window and execute `npm start`

You should now be able to navigate to http://localhost:3000 and view the UI.

You should also be able to communicate with the API at http://localhost:3000/api/games

If you get an error like this when trying to build the project: `ERROR: Please install sqlite3 package manually` you should run `npm rebuild` from the project root.

# Practical Assignments
Pretend for a moment that you have been hired to work at Voodoo.  You have grabbed your first tickets to work on an internal game database application. 

#### FEATURE A: Add Search to Game Database
The main users of the Game Database have requested that we add a search feature that will allow them to search by name and/or by platform.  The front end team has already created UI for these features and all that remains is for the API to implement the expected interface.  The new UI can be seen at `/search.html`

The new UI sends 2 parameters via POST to a non-existent path on the API, `/api/games/search`

The parameters that are sent are `name` and `platform` and the expected behavior is to return results that match the platform and match or partially match the name string.  If no search has been specified, then the results should include everything (just like it does now).

Once the new API method is in place, we can move `search.html` to `index.html` and remove `search.html` from the repo.

#### FEATURE B: Populate your database with the top 100 apps
Add a populate button that calls a new route `/api/games/populate`.  This route should populate your database with the top 100 games in the app store and the google play store.  You are free to use any mean necessary to get and store the information.  Please remember that simple solutions are preferred.


# Theory Assignments
You should complete these only after you have completed the practical assignments.

The business goal of the game database is to provide an internal service to get data for all apps from all app stores.  Many other applications at Voodoo will use this API when they need to get app data. 

#### Question 1
For you what is missing in the project to make it production ready?

For this project to be considered production-ready the following things should be done:
- Add an authentication layer to make sure only authorized users can access the API.
- Ensure every feature is covered by unit/integration tests.
- Add a health check endpoint (this will come in handy when deploying to Kubernetes).
- Replace the SQLite database with a database better suited for concurrent write access (PostgreSQL might be a good fit).
- Create targeted database indexes to improve performances. Search performance and relevancy, for example, could be improved by creating an index on a vector column computed from the name of the app.
- Ensure features and APIs are properly documented.
- Write a Dockerfile to encapsulate the app in a Docker container.
- Create the infrastructure (PostgreSQL database and Kubernetes service) needed to run the app (preferably with an infra as code approach using Terraform).
- Setup a CI/CD pipeline to build, test and deploy the app automatically.
- Install an APM agent alongside the container to ensure performances are monitored.
- Ensure logs are properly collected and meaningful alerts are setup.

#### Question 2
To achieve the final business goal what is your Roadmap and Action plan?

First of all, since this API is going to be used by a multitude of applications, we should start by asking key stakeholders what they expect from it, and what they would like to see included in its scope. This will allow us to compile a list of features that might be worth developing.

Once we have a general feel for what feature we will need to provide. We will search the market to learn about potential solution vendors and thirds party APIs. Should those APIs cover our functional requirements, we can follow up with a cost analysis to choose between building the API inhouse vs subscribing to a paid plan.

Should we decide to build the API ourself, we should then start refining the feature list we compiled during the first step.
Once we refined this list, we can draft the roadmap.

A potential roadmap might look like this:

- Milestone 1: The API is live, but restricted to a single store (App Store or Play Store). The database contains only a portion of the apps (95% most downloaded for example) catalogue and is synched only once every few days.
- Milestone 2: The API now supports both major app stores (database limitation still apply).
- Milestone 3: The database is now exhaustive and contains every app.
- Milestone 4: The database is refreshed hourly

From there on, if every key stakeholder signs off on the roadmap, we will be able to start the planning of the iterative development of the API.